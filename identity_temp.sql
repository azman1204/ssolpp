-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for sso
CREATE DATABASE IF NOT EXISTS `sso` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sso`;

-- Dumping structure for table sso.identity_temp
CREATE TABLE IF NOT EXISTS `identity_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `source_app` varchar(50) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table sso.identity_temp: ~5 rows (approximately)
DELETE FROM `identity_temp`;
/*!40000 ALTER TABLE `identity_temp` DISABLE KEYS */;
INSERT INTO `identity_temp` (`id`, `name`, `user_id`, `email`, `source_app`) VALUES
	(1, 'Abu Hasrul bin Karim', '840412095063', 'abu@gmail.com', 'domain1'),
	(2, 'Ali bin Bakr', '123456789012', 'ali@yahoo.com', 'domain1'),
	(3, 'John Doe', '940412095063', 'john@gmail.com', 'domain2'),
	(4, 'Abu Hasrul bin Karim', '840412095063', 'abu@gmail.com', 'domain2'),
	(5, 'Abu Hasrul bin Karim', '840412095063', 'abu@gmail.com', 'domain3');
/*!40000 ALTER TABLE `identity_temp` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
