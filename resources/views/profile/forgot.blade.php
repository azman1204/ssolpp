@extends('layout_public')
@section('header', 'Forgot Password')
@section('content')

    <form action="/forgot-password" method="post">
        @csrf
        <div class="row">
            <div class="col-md-4">
                @if (session()->has('msg'))
                <div class="alert alert-danger">{{ session('msg') }}</div>
                @endif

                <label>ID Pengguna</label>
                <input type="text" " class="form-control" name="user_id">
                <input type="submit" class="btn btn-primary">
            </div>
        </div>
    </form>

@endsection
