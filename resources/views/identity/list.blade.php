@extends('layout')
@section('header', 'Senarai Pengguna')
@section('content')

<a href="/identity/create" class="btn btn-primary mb-1"><span data-feather="plus"></span>Tambah</a>

<table class="table table-borded table-striped table-hover">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Nama</th>
            <th>Id Pengguna</th>
            <th>Email</th>
            <th class="text-center">Tindakan</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($indentities as $identity )
        <tr>
            <td>{{ $loop->iteration}}</td>
            <td>{{ $identity->name}}</td>
            <td>{{ $identity->user_id}}</td>
            <td>{{ $identity->email}}</td>
            <td align="center">
                <a href="/identity/edit/{{ $identity->id }}" class="btn btn-primary btn-sm" >Kemaskini</a>
                <a href="/identity/delete/{{ $identity->id }}" onclick="return confirm('Adakah Anda Pasti ??')" class="btn btn-danger btn-sm" >Hapus</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection
