@extends('layout')
@section('header', 'Daftar Pengguna')
@section('content')

@foreach ($errors->all() as $error)
    <li class="text-danger">{{ $error }}</li>
@endforeach

<form action="/identity/save" method="post">

    @csrf
    <input type="hidden" name="id" value="{{ old('id', $identity->id) }}">

    <div class="row" mb-3>
        <div class="col-md-2">Jenis Pengguna Pengguna</div>
        <div class="col-md-18">
            {{ Form::select('role', ['0'=>'-- Sila Pilih --', '1' => 'Admin', '2' => 'Pengguna'],
             old('role', $identity->role), ['class'=>'form-control']) }}
        </div>
    </div>

    <div class="row" mb-3>
        <div class="col-md-2">Id Pengguna</div>
        <div class="col-md-18">
            <input type="text" class="form-control" name="user_id" value="{{ old('user_id', $identity->user_id ) }}">
        </div>
    </div>

    <div class="row" mb-3>
        <div class="col-md-2">KataLaluan</div>
        <div class="col-md-18">
            <input type="password" class="form-control" name="password" value="{{ old('password', '********') }}">
        </div>
    </div>

    <div class="row" mb-3>
        <div class="col-md-2">Pengesahan KataLaluan</div>
        <div class="col-md-18">
            <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation', '********') }}">
        </div>
    </div>

    <div class="row" mb-3>
        <div class="col-md-2">Nama</div>
        <div class="col-md-12">
            <input type="text" class="form-control" name="name" value="{{ old('name', $identity->name ) }}">
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-2">Email</div>
        <div class="col-md-12">
            <input type="text" class="form-control" name="email" value="{{ old('email', $identity->email ) }}">
        </div>
    </div>

    <!-- senarai applikasi disini -->
    <div class="row mb-3">
        <div class="col-md-2">Aplikasi</div>
        <div class="col-md-12">
            @foreach ($apps as $app)
                <input type="checkbox" value="{{ $app->id }}"
                {{ in_array($app->id, $iapp) ? 'checked' : '' }}
                name="app[]"> {{ $app->name }} <br>
            @endforeach
        </div>
    </div>

    <div class="row mb-3">
        <div class="col-md-2"></div>
        <div class="col-md-12">
            <input type="submit" value="Simpan" class="btn btn-primary">
        </div>
    </div>
</form>
@endsection
