<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Ref extends Model {
    // return array cth ['1' => 'domain 1', '2' => 'domain 2']
    public static function getRef($cat) {
        return self::where('cat', $cat)->pluck('name', 'code');
    }
}
