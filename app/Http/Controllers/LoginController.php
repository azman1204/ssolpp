<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller {
    //encrypt dan decrypt data
    function hashString($data, $method='encrypt') {
        $secret = '123456';
        $iv = substr(hash('sha256', $secret), 0, 16);

        if ($method == 'encrypt') {
            $str = openssl_encrypt($data, "AES-256-CBC", "$secret", 0, $iv);
        } else if ($method == 'decrypt'){
            $str = openssl_decrypt($data, "AES-256-CBC", "123456",0, $iv);
        }
        return $str;
    }

    function verify(Request $request) {
        // Ini untuk elak user directly datang kelogin sso page
        if (! session()->has('return_url')) {
            if (! $request->has('return_url')) {
                echo 'TIADA KEBENARAN ...';
                exit;
            }
        }

        if ($request->has('return_url')) {
            //come from domain / SP
            $return_url = $this->hashString($request->return_url, 'decrypt');
            if (empty($return_url)){
                // CUBAAN TAMPER DATA
                echo 'TECHNICAL ERROR ....';
                exit;
            }
        } else {
            // login but faild
            $return_url = session('return_url');
        }

        // Check User ini sudah ada cookie ke Belum?
        if ($request->hasCookie('ssoapp')) {
            // jika ada redirect back to inteded system to enter
            //return redirect('http://domain1.test/accept?user_id=abu');
            $user_id = urlencode($request->cookie('ssoapp'));
            $user_id2 = urlencode($this->hashString($user_id));
            $return_url = $this->hashString($request->return_url, 'decrypt');
            return redirect($return_url. '?user_id=' . $user_id2);
        } else {
            //jika tiada, show login screenshots
            $return_url = $this->hashString($request->return_url, 'decrypt');
            session(['return_url' => $return_url]);
            return view('login_sso');
        }
    }

    function authsso(Request $request){
        $credentials = [
            'user_id' => $request->user_id,
            'password' => $request->password
        ];
        // dd($credentials);

        $cookie = cookie('ssoapp', $request->user_id, 24 * 60);
        $user_id = urlencode($this->hashString($request->user_id));

        if (Auth::attempt($credentials)) {
            //Berjaya Login
            //echo 'berjaya';

            $cookie = cookie('ssoapp', $request->user_id, 24 * 60);
            $user_id = urlencode($this->hashString($request->user_id));
            return redirect(session('return_url') . '?user_id=' . $user_id)->withCookie($cookie);
        } else {
            //Gagal Login
            //echo 'gagal';
            return redirect('verify')->with('msg', 'Login Gagal');
            //with() -> flash session, guna untuk papar message
        }
    }

    function login() {
        return view('login');
    }

    // authentication utk masuk ke SSO app
    function auth(Request $request) {
        $credentials = [
            'user_id' => $request->user_id,
            'password' => $request->password
        ];

        $identity = \App\Models\Identity::where('user_id', $request->user_id)->first();
        if (! $identity) {
            // user tidak wujud
            return redirect('login')->with('msg', 'Login Gagal');
        }

        if (Auth::attempt($credentials)) {
            //Berjaya Login, check kena change password ataupun tidak
            if ($identity->change_pwd == 1)
                return redirect('/password/change');

            $cookie = cookie('ssoapp', $request->user_id, 24 * 60);
            return redirect('dashboard')->withCookie($cookie);
        } else {
            //Gagal Login
            return redirect('login')->with('msg', 'Login Gagal');
        }
    }

    // logout melalui portal SSO
    function logout() {
        Auth::logout();
        $cookie = cookie('ssoapp', '', -1); // destroy cookie
        \Cookie::forget('ssoapp');
        return redirect('/login')->withCookie($cookie);
    }

    // logout melalui domain1.test, domain2.test, ...
    function logoutSso() {
        Auth::logout(); // logout sso
        $cookie = cookie('ssoapp', '', -1);
        \Cookie::forget('ssoapp');
        //return redirect('logout-msg')->withCookie($cookie);
        return redirect('/login')->withCookie($cookie);
    }

    function logoutmsg() {
        return view('logout-sso');
    }

 }
