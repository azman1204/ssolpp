<?php
namespace App\Http\Controllers;

use App\Models\Identity;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PasswordController extends Controller {
    function index() {
        return view('password.index');
    }

    function post(Request $req) {
        $domain = $req->domain; // 1, 2, 3
        // query ke table identity_temp, baca semua data mengikut domain
        $rows = \DB::table('identity_temp')->where('source_app', $domain)->get();
        foreach ($rows as $identity) {
            // generate random pwd
            echo $identity->name;
            $str = Str::random(15);
            $pwd = \Hash::make($str);
            $iden = Identity::where('user_id', $identity->user_id)->first();
            $iden->password = $pwd;
            $iden->change_pwd = 1;
            $iden->save();

            // emel kpd user tersebut
            \Mail::send('mail.rand_pwd',
            ['identity' => $identity, 'pwd' => $str],
            function($m) use ($iden) {
                $m->to($iden->email);
                $m->from('admin@sso.test');
                $m->subject('Katalaluan SSO');
            });
        }
    }

    // show form to change password
    function change() {
        return view('password.change');
    }

    // save a new password
    function changeHandler(Request $request) {
        $rules = [
            'password' => 'required|min:6|max:20|confirmed',
        ];
        $request->validate($rules);
        $identity = \Auth::user();
        $identity->password = \Hash::make($request->password);
        $identity->change_pwd = 0;
        $identity->save();
        return redirect('/login')->with('msg', 'Katalaluan telah berjaya dikemaskini');
    }
}
